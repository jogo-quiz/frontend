const urlUsuario = "http://167.99.108.187:8082/jogador/";
const urlJogo = "http://167.99.108.187:8081/jogo/iniciar/";

let userid = localStorage.getItem("userid");

function carregarPerfil() {
    if (localStorage.getItem("token") == null || localStorage.getItem("token") == "") {
        window.location.href = "../index.html";
    }
    let userid = localStorage.getItem("userid");
    fetch(urlUsuario+userid, montarGet()).then(extrairJSON).then(tratarRetornoCarregarPerfil).catch(tratarErro);
}

function tratarRetornoCarregarPerfil(retorno) {
    if(retorno) {
        let hiheader = document.createElement("h3");
        hiheader.innerHTML = `Olá, ${retorno.nome}!`;
        div_ola.appendChild(hiheader);
    }
}

function toPerguntas() {
    //let userid = localStorage.getItem("userid");
    fetch(urlJogo+userid, montarGet()).then(extrairJSON).then(tratarRetornoMontarJogo).catch(tratarErro);
    //fetch("http://www.mocky.io/v2/5b71c6c53200000f00f36f9e").then(extrairJSON).then(tratarRetornoMontarJogo).catch(tratarErro);
    //loadJSON("../mock/sessao1.json", tratarRetornoMontarJogo);
    //location.href = "../perguntas/perguntas.html";
}

function tratarRetornoMontarJogo(retorno) {
    console.log(retorno);
    if(retorno) {
        localStorage.setItem("sessaoid", retorno.id);
        //location.href = "../perguntas/perguntas.html";
    }
    else
        alert("deu ruim");
}

btn_iniciar.onclick = toPerguntas;
anc_sair.onclick = limparStorage;
window.onload = carregarPerfil;
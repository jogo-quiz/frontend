const urllogin="http://167.99.108.187:8082/jogador/login";

function realizarLogin(dados) {
    let usuario = {
        nome: inp_nome.value,
        senha: inp_senha.value
    };
    let postmontado = montarPost(usuario);
    fetch(urllogin, postmontado).then(extrairJSON).then(retornoLogin).catch(tratarErro);
}

function retornoLogin(retorno) {
    alert("Login realizado com sucesso.")
    localStorage.setItem("userid", retorno.id);
    window.location.href = "inicial/inicial.html";
}

function toCadastrar() {
    location.href = "cadastro/cadastro.html";
}

btn_logon.onclick = realizarLogin;
btn_cadastrar.onclick = toCadastrar;
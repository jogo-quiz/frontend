const urlUsuario = "http://167.99.108.187:8082/jogador/";
//const urlJogo = "http://142.93.23.164:8081/jogo/";
const urlPerguntas = "http://142.93.23.164:8081/jogo/pergunta/";

let sessaoid = localStorage.getItem("sessaoid");

function carregarPerfil() {
    if (localStorage.getItem("token") == null) {
        window.location.href = "index.html";
    }
    let userid = localStorage.getItem("userid");
    let getmontado = montarGet();
    fetch(urlUsuario+userid, getmontado).then(extrairJSON).then(tratarRetornoCarregarPerfil).catch(tratarErro);
}

function tratarRetornoCarregarPerfil(retorno) {
    //console.log(retorno);
    usuario = retorno;
    carregarPerguntas();
}

function carregarPerguntas() {
    //console.log("carregarPerguntas");
    console.log(sessaoid);
    fetch(urlPerguntas+sessaoid, montarGet()).then(extrairJSON).then(tratarRetornoPerguntas).catch(tratarErro);
    //fetch("http://www.mocky.io/v2/5b71c7cd3200005519f36fa1", montarGet()).then(extrairJSON).then(tratarRetornoPerguntas).catch(tratarErro);
    //loadJSON("../mock/pergunta1.json",tratarRetornoPerguntas);
}

function tratarRetornoPerguntas(retorno) {
    /*console.log("tratarRetornoPerguntas");
    console.log(retorno);*/
    let hPergunta = document.createElement("h3");
    hPergunta.innerHTML = retorno.pergunta;
    frm_questionario.appendChild(hPergunta);

    for (let index = 0; index < retorno.respostas.length; index++) {
        let inputResposta = document.createElement("input");
        inputResposta.setAttribute('type', 'radio');
        inputResposta.setAttribute('name', 'resposta');
        inputResposta.setAttribute('id', 'inp_resp' + retorno.respostas[index].id);
        inputResposta.setAttribute('value', retorno.respostas[index].id);
        //inputResposta.onchange = radioButtonClicks;

        let labelResposta = document.createElement("label");
        labelResposta.innerHTML = "<span> " + retorno.respostas[index].resposta + "</span><br>";

        frm_questionario.appendChild(inputResposta);
        frm_questionario.appendChild(labelResposta);
    }
}

/*function radioButtonClicks() {
    console.log(this.id + " - " + this.checked);
    const radios = document.querySelectorAll("input");
    for (let index = 0; index < radios.length; index++) {
        let radio = radios[index];
        console.log(radio.id + " - " + radio.checked);
        console.log(radio === this);
    }
    console.log("  ");
}*/

function responderQuestao() {
    const radios = document.querySelectorAll("input");
    for (let index = 0; index < radios.length; index++) {
        let radio = radios[index];
        if(radio.checked === true){
            console.log(radio.value + " selecionado");
        }
    }
}

btn_responder.onclick = responderQuestao;
anc_sair.onclick = limparStorage;
window.onload = carregarPerguntas;
const urlCadastro = "http://167.99.108.187:8082/jogador";

let cor_texto = "red";

function validarEmail() {
    let email = inp_username.value;

    if (email !== "" && (email.length < 5)) {
        return false;
    }
    return true;
}

function validarSenha() {
    let senha = inp_senha.value.toUpperCase();
    let numeros = /[0-9]/g;
    let letras = /[A-Z]/g;
    let retorno = false;

    let tamanho = senha.length > 1;
    let temNumero = senha.match(numeros) !== null;
    let temLetras = senha.match(letras) !== null;

    retorno = (tamanho) || senha === "";

    console.log(senha + " - " + retorno);
    return retorno;
}

function eventoEmail() {
    if (validarEmail()) {
        lbl_nome.style.display = "none";
    }
    else {
        lbl_nome.style.display = "block";
    }
}

function eventoSenha() {
    if (validarSenha()) {
        lbl_senha.style.display = "none";
    }
    else {
        lbl_senha.style.display = "block";
    }
}

function realizarCadastro(dados) {
    let jogador = {
        nome : inp_username.value,
        senha : inp_senha.value
    };
    let postmontado = montarPost(jogador);
    console.log(JSON.stringify(postmontado));
    fetch(urlCadastro, postmontado).then(extrairJSON).then(retornoCadastro).catch(tratarErro);
}

function retornoCadastro(retorno) {
    console.log(retorno);
    alert("Jogador criado com sucesso");
    location.href = "../index.html";
}

function toIndex() {
    location.href = "../index.html";
}

inp_username.onblur = eventoEmail;
inp_senha.onblur = eventoSenha;

btn_finalizar.onclick = realizarCadastro;
btn_voltar.onclick = toIndex;
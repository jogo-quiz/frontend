function tratarErro(params) {
    console.log("Problemas ao realizar chamada - " + params);
}

function extrairJSON(resposta) {
    console.log(resposta);
    console.log("Extraindo JSON. Resposta: " + resposta.status);
    if(resposta.status === 200){
        console.log(resposta.headers.get("Authorization"));
        localStorage.setItem("token", resposta.headers.get("Authorization"));
        return resposta.json();
    }
    else {
        console.log("Resposta inesperada: " + resposta);
        return resposta.json(); 
    }
}

function montarPost(data){
    let token = localStorage.getItem("token");
    return {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': token
        }
    };
}

function montarGet(){
    let token = localStorage.getItem("token");
    return {
        method: 'GET',
        headers: {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': token
        }
    };
}

function limparStorage() {
    localStorage.setItem("token", "");
    localStorage.setItem("sessionid", "");
    localStorage.setItem("userid", "");
    console.log("Sessão encerrada");
}

function loadJSON(filend,callback) {   

    console.log(filend);
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', filend, true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(JSON.parse(xobj.responseText));
        }
    };
    
    xobj.send(null);  
 }